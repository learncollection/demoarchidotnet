﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleService.Data
{
    public class TrainSchedule
    {
        public Guid Id { get; set; }

        [Required]
        public string Destination { get; set; }

        public DateTime DepartureTime { get; set; }
    }
}
