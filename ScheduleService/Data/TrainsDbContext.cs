﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleService.Data
{
    public class TrainsDbContext : DbContext
    {
        public TrainsDbContext(DbContextOptions<TrainsDbContext> dbContext) : base(dbContext)
        {
        }

        public DbSet<TrainSchedule> Schedules { get; set; }
    }
}
