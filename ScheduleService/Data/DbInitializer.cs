﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleService.Data
{
    public class DbInitializer
    {
        private readonly TrainsDbContext dbContext;

        public DbInitializer(TrainsDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Initialize()
        {
            if(dbContext.Schedules.Any() == false)
            {
                for (int i = 0; i < 10; i++)
                {
                    dbContext.Schedules.Add(new TrainSchedule()
                    {
                        Destination = i % 2 == 0 ? "Toulouse" : "Lyon",
                        DepartureTime = DateTime.Now.AddHours(2 * i)
                    });
                }

                dbContext.SaveChanges();
            }


        }
    }
}
