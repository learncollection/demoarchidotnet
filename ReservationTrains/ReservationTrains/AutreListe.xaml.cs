﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReservationTrains
{
    /// <summary>
    /// Logique d'interaction pour AutreListe.xaml
    /// </summary>
    public partial class AutreListe : Window
    {
        public AutreListe()
        {
            InitializeComponent();
        }

        private async void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var schedules = LoadTrains();

            listBox.ItemsSource = await schedules;

        }

        async Task<IEnumerable<TrainSchedule>> LoadTrains()
        {
            string url = "http://13.77.157.230/api/TrainSchedulesApi";
            var client = new System.Net.Http.HttpClient();
            var response = await client.GetAsync(url);
            string jsonString = await response.Content.ReadAsStringAsync();
            return Newtonsoft.Json.JsonConvert.DeserializeObject<TrainSchedule[]>(jsonString);
        }
    }
}
