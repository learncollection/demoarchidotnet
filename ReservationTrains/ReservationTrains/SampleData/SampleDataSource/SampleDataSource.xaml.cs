﻿//      *********    NE PAS MODIFIER CE FICHIER     *********
//      Ce fichier est régénéré par un outil de création.Modifier
// .     ce fichier peut provoquer des erreurs.
namespace Expression.Blend.SampleData.SampleDataSource
{
    using System; 
    using System.ComponentModel;

// Pour réduire de façon significative l'empreinte des exemples de données dans votre application de production, vous pouvez définir
// la constante de compilation conditionnelle DISABLE_SAMPLE_DATA et désactiver les données échantillons lors de l'exécution.
#if DISABLE_SAMPLE_DATA
    internal class SampleDataSource { }
#else

    public class SampleDataSource : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public SampleDataSource()
        {
            try
            {
                Uri resourceUri = new Uri("ms-appx:/SampleData/SampleDataSource/SampleDataSource.xaml", UriKind.RelativeOrAbsolute);
                System.Windows.Application.LoadComponent(this, resourceUri);
            }
            catch
            {
            }
        }

        private ItemCollection _Collection = new ItemCollection();

        public ItemCollection Collection
        {
            get
            {
                return this._Collection;
            }
        }
    }

    public class Item : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string _Destination = string.Empty;

        public string Destination
        {
            get
            {
                return this._Destination;
            }

            set
            {
                if (this._Destination != value)
                {
                    this._Destination = value;
                    this.OnPropertyChanged("Destination");
                }
            }
        }

        private string _DepartureTime = string.Empty;

        public string DepartureTime
        {
            get
            {
                return this._DepartureTime;
            }

            set
            {
                if (this._DepartureTime != value)
                {
                    this._DepartureTime = value;
                    this.OnPropertyChanged("DepartureTime");
                }
            }
        }

        private double _Longitude = 0;

        public double Longitude
        {
            get
            {
                return this._Longitude;
            }

            set
            {
                if (this._Longitude != value)
                {
                    this._Longitude = value;
                    this.OnPropertyChanged("Longitude");
                }
            }
        }

        private double _Latitude = 0;

        public double Latitude
        {
            get
            {
                return this._Latitude;
            }

            set
            {
                if (this._Latitude != value)
                {
                    this._Latitude = value;
                    this.OnPropertyChanged("Latitude");
                }
            }
        }
    }

    public class ItemCollection : System.Collections.ObjectModel.ObservableCollection<Item>
    { 
    }
#endif
}
