﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationTrains
{
    public class TrainSchedule
    {
        Random r = new Random((int)DateTime.Now.Ticks);

        public TrainSchedule()
        {
            Longitude = r.Next(200);
            Latitude = r.Next(200);
        }

        public Guid Id { get; set; }
        public string Destination { get; set; }
        public DateTime DepartureTime { get; set; }

        public double Longitude { get; set; }
        
        public double Latitude { get; set; }
    }
}
